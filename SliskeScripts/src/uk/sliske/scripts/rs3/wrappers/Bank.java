package uk.sliske.scripts.rs3.wrappers;

import org.powerbot.script.Tile;
import org.powerbot.script.rt6.ClientContext;

import uk.sliske.scripts.rs3.Debug;
import uk.sliske.scripts.rs3.util.ItemGroup;


public class Bank extends org.powerbot.script.rt6.Bank {

	public Bank(ClientContext ctx) {
		super(ctx);
		Debug.println("custom banking class loaded");
	}

	public void walkTo() {
		ctx.movement.findPath(nearestTile()).traverse();
		// ctx.movement.newTilePath(arg0)
	}

	private Tile nearestTile() {
		return nearest().tile();
	}

	public int countItem(final int id) {
		return select().id(id).count(true);
	}

	public boolean bankAll(){
		return bankAll(false);
	}
	
	public boolean bankAll(final boolean closeAfter) {
		if (open()) {
			if (depositInventory()) {
				if (closeAfter) {
					if (close())
						return true;
				}
				return true;
			}
		}
		return false;
	}
	
	public boolean withdraw(final int id, final int amount){
		open();
		return super.withdraw(id, amount);
	}
	
	public boolean withdraw(final ItemGroup item){		
		return withdraw(item.getID(),item.getAmount());
	}

}
