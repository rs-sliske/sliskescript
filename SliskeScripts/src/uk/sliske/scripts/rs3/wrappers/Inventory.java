package uk.sliske.scripts.rs3.wrappers;

import java.util.ArrayList;

import org.powerbot.script.rt6.Backpack;

import uk.sliske.scripts.rs3.Debug;
import uk.sliske.scripts.rs3.util.ItemGroup;


public class Inventory extends Backpack {

	public Inventory(final ClientContext ctx) {
		super(ctx);
		Debug.println("custom inventory class loaded");
	}

	public boolean isFull() {
		return select().count() == 28;
	}

	public boolean isEmpty() {
		return select().count() == 0;
	}

	public int count(final int itemID) {
		return select().id(itemID).count();
	}

	public boolean contains(final int itemID) {
		return count(itemID) > 0;
	}

	public ArrayList<ItemGroup> inventoryList() {
		Debug.println("attempting to load inventory");
		ArrayList<ItemGroup> res = new ArrayList<ItemGroup>();
		ArrayList<Integer> ids = new ArrayList<Integer>();
		select();
		while (!super.isEmpty()) {
			int id = poll().id();
			ids.add(id);
			while (peek().id() == id) {
				poll();
			}
		}
		for (Integer i : ids) {
			res.add(new ItemGroup(i, count(i)));
		}
		Debug.println("inventory loaded");
		return res;
	}
}
