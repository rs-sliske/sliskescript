package uk.sliske.scripts.rs3.wrappers;

import java.io.FileNotFoundException;
import org.powerbot.script.rt6.Player;

import uk.sliske.scripts.rs3.Debug;
import uk.sliske.scripts.rs3.util.GrandExchange;

import com.sk.cache.DataSource;
import com.sk.cache.fs.CacheSystem;
import com.sk.cache.wrappers.ItemDefinition;
import com.sk.cache.wrappers.loaders.ItemDefinitionLoader;

public class ClientContext extends org.powerbot.script.rt6.ClientContext {
	public final Inventory			invent;
	public final Player				local;
	public final Bank				bank;
	private boolean					cacheLoaded	= false;
	private CacheSystem				cache;
	private ItemDefinitionLoader	itemLoader;

	public ClientContext(org.powerbot.script.rt6.ClientContext ctx) {
		super(ctx);
		Debug.println("initialising custom client context");
		this.invent = new Inventory(this);
		GrandExchange.init();
		this.local = players.local();
		this.bank = new Bank(this);
		try {
			Debug.println("attempting to load cache");
			cache = new CacheSystem(new DataSource(DataSource.getDefaultCacheDirectory()));
			itemLoader = new ItemDefinitionLoader(cache);
			cacheLoaded = true;
			Debug.println("cache loaded successfully");
		} catch (FileNotFoundException e) {
			cacheLoaded = false;
			Debug.println("cache loading failed");
		}
	}

	public ItemDefinition getItemInfo(final int itemID) {
		ItemDefinition res = itemLoader.load(itemID);

		return res;
	}

	public boolean cacheLoadedSuccessfully() {
		return cacheLoaded;
	}
}
