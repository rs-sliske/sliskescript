package uk.sliske.scripts.rs3.util;

public class ItemGroup {

	private int		itemID;
	private int		amount;
	private boolean	stacks	= false;

	public ItemGroup(final int id, final int amount, final boolean stacks) {
		this.itemID = id;
		this.amount = amount;
		this.stacks = stacks;
	}

	public ItemGroup(final int id, final int amount) {
		this(id, amount, false);
	}
	
	public int getID() {
		return itemID;
	}

	public int getAmount() {
		return amount;
	}
	
	public boolean stacks(){
		return stacks;
	}

}
