package uk.sliske.scripts.rs3.util;

import java.util.ArrayList;

import org.powerbot.script.rt6.ClientContext;

import uk.sliske.scripts.rs3.Debug;
import uk.sliske.scripts.rs3.util.Timer.FORMAT;


public class Tracker {
	private static Tracker			instance;
	private ClientContext			ctx;
	private static final String[]	SKILL_NAMES	= { "Attack", "Defence", "Strength", "Hitpoints", "Ranged", "Prayer", "Magic", "Cooking",
			"Woodcutting", "Fletching", "Fishing", "Firemaking", "Crafting", "Smithing", "Mining", "Herblore", "Agility", "Thieving", "Slayer",
			"Farming", "Runecrafting", "Hunter", "Construction", "Summoning", "Dungeoneering", "Divination" };
	private int						totalXpGained;
	private int						totalXpPerHour;
	private int						totalLevelsGained;
	private ArrayList<Skill>		unusedSkills;
	private ArrayList<Skill>		usedSkills;
	private ArrayList<Skill>		temp;

	public static Tracker get(ClientContext ctx) {
		if (instance == null) {
			instance = new Tracker(ctx);
		}
		return instance;
	}

	public static Tracker get() {
		return instance;
	}

	private Tracker(ClientContext ctx) {
		this.ctx = ctx;
		usedSkills = new ArrayList<Skill>();
		unusedSkills = new ArrayList<Skill>();
		temp = new ArrayList<Skill>();
		for (int i = 0; i < 26; i++) {
			Skill s = new Skill(SKILL_NAMES[i], i, ctx.skills.experience(i), ctx.skills.level(i));
			unusedSkills.add(s);
		}
	}

	public void update() {
		final double tempTime = getTimeVar();
		totalXpGained = 0;
		totalLevelsGained = 0;
		for (Skill s : unusedSkills) {
			s.update(ctx.skills.experience(s.getID()), ctx.skills.level(s.getID()));
			if (s.isUsed()) {
				usedSkills.add(s);
				temp.add(s);
			}
		}
		for (Skill s : usedSkills) {
			s.update(ctx.skills.experience(s.getID()), ctx.skills.level(s.getID()));
			s.calcXpRate(tempTime);
			totalXpGained += s.getXpGained();
			totalLevelsGained += s.getLevelsGained();
		}
		totalXpPerHour = (int) ((totalXpGained / tempTime));
		Debug.println(totalXpGained+"");
		for (Skill s : temp) {
			unusedSkills.remove(s);
		}
		temp.clear();
	}

	private double getTimeVar() {
		return Timer.ranFor(FORMAT.MILI) * 3600;
	}

	public int getTotalXpGained() {
		return totalXpGained;
	}

	public int getTotalXpPerHour() {
		return totalXpPerHour;
	}

	public int getTotalLevelsGained() {
		return totalLevelsGained;
	}

	public ArrayList<Skill> getUsedSkills() {
		return usedSkills;
	}

}
