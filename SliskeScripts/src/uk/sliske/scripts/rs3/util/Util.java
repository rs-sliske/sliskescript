package uk.sliske.scripts.rs3.util;


public class Util {
	
	public static int intArrayTotal(int[] args) {
		int res = 0;
		for (int i : args) {
			res += i;
		}
		return res;
	}
	
}
