package uk.sliske.scripts.rs3.util;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class Paint {

	private TAB					currentTab	= TAB.XP;
	private ArrayList<Skill>	activeSkills;
	private int					xOffset		= 10;
	private int					yOffset		= 400;

	enum TAB {
		MONEY, XP, GENERAL;
	}

	public void draw(final Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(xOffset, yOffset, 200, 100);
		g.setColor(Color.red);
		switch (currentTab) {
			case MONEY:

			case XP:
				drawXP(g);
			case GENERAL:

		}
	}

	public void drawXP(final Graphics g) {
		activeSkills = Tracker.get().getUsedSkills();
		int row = 0;
		if (!activeSkills.isEmpty())
			for (Skill s : activeSkills) {
				row++;
				g.drawString(s.getName(), xOffset + 10, yOffset + (row * 20));
				g.drawString("Gained : " + s.getXpGained(), xOffset + 70, yOffset + (row * 20));
				g.drawString("XP/H : " + s.getXpRate(), xOffset + 150, yOffset + (row * 20));				
			}
	}

}
