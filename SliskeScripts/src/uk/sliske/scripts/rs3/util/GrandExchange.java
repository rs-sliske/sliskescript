package uk.sliske.scripts.rs3.util;

import java.util.HashMap;

import org.powerbot.script.rt6.GeItem;

import uk.sliske.scripts.rs3.Debug;


public class GrandExchange {

	private static HashMap<Integer, GeItem>	items;
	

	public static void init() {
		items = new HashMap<Integer, GeItem>();		
		Debug.println("custom ge class loaded");
	}

	public static int getPrice(final int itemID) {
		if (!items.containsKey(itemID)) {
			items.put(itemID, GeItem.profile(itemID));
		}
		return items.get(itemID).price(GeItem.PriceType.CURRENT).price();
	}
	
	public static void refreshList(){
		items.clear();
	}

}
