package uk.sliske.scripts.rs3.util;

public final class Skill {

	private final String	name;
	private final int		id;

	private final int		startXP;
	private int				curXP;

	private final int		startLVL;
	private int				curLVL;

	private int				xpRate;

	private boolean			used	= false;

	public Skill(final String name, final int id, final int xp, final int lvl) {
		this.name = name;
		this.id = id;
		this.startXP = xp;
		this.curXP = xp;
		this.startLVL = lvl;
		this.curLVL = lvl;
		xpRate = 0;
	}

	public void update(final int xp, final int lvl) {
		this.curXP = xp;
		this.curLVL = lvl;
		used = curXP > startXP;
	}

	public int getLevelsGained() {
		if (!used)
			return 0;
		return curLVL - startLVL;
	}

	public void calcXpRate(final double time) {
		if (time > 0)
			xpRate = (int) (getXpGained() / time);
	}

	public int getXpGained() {
		if (!used)
			return 0;
		return curXP - startXP;
	}

	public String getName() {
		return name;
	}

	public int getID() {
		return id;
	}
	
	public int getXpRate(){
		return xpRate;
	}
	public boolean isUsed(){
		return used;
	}

}
