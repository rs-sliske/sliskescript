package uk.sliske.scripts.rs3;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Callable;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.event.MenuEvent;

import org.powerbot.script.BotMenuListener;
import org.powerbot.script.Condition;
import org.powerbot.script.PaintListener;
import org.powerbot.script.PollingScript;
import org.powerbot.script.Random;

import uk.sliske.scripts.rs3.util.Paint;
import uk.sliske.scripts.rs3.util.Tracker;
import uk.sliske.scripts.rs3.wrappers.ClientContext;


public abstract class SliskeScript<C extends ClientContext> extends PollingScript<C> implements PaintListener, BotMenuListener {
	protected boolean		running;
	protected final Tracker	tracker;
	protected Paint			paint;	

	protected SliskeScript() {
		Debug.initFrame();
		tracker = Tracker.get(ctx);
		manageTracker();
		paint = new Paint();
	}
	protected SliskeScript(final boolean debug) {
		this();
		Debug.enabled(debug);
	}
	protected void loadGUI() {
		Debug.println("no GUI available");
	}

	@Override
	public abstract void start();

	@Override
	public abstract void poll();

	public void error(final String message) {
		Debug.println(message);
	}

	private final void manageTracker() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Condition.wait(new Callable<Boolean>() {
					@Override
					public Boolean call() throws Exception {
						return running;
					}
				});
				while (running) {
					tracker.update();
					Debug.println("tracker updated");
					try {
						Thread.sleep(6000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

	public void repaint(Graphics g) {
		paint.draw(g);
	}

	protected int genRandWait(final int mean, final int variance) {
		double temp = Random.nextGaussian()*variance;
		temp+=mean;
		return (int) Math.abs(temp);
	}
	

	@Override
	public void menuCanceled(MenuEvent e) {
		
	}

	@Override
	public void menuDeselected(MenuEvent e) {
		
	}

	@Override
	public void menuSelected(MenuEvent e) {
		final JMenu menu = (JMenu) e.getSource();
		final JMenuItem gui = new JMenuItem("Toggle Debug Window");
		gui.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Debug.toggle();
			}
		});
		menu.add(gui);
	}
}