package uk.sliske.scripts.rs3.transport.methods;

import uk.sliske.scripts.rs3.Debug;

public class FairyRing {

	private final char left;
	private final char middle;
	private final char right;
	
	public FairyRing(final String code){
		if(code.length()!=3){
			Debug.println("invalid fairy ring code : "+code);
		}
		this.left = code.charAt(0);
		this.middle = code.charAt(1);
		this.right = code.charAt(2);
	}

	public char getLeft() {
		return left;
	}

	public char getMiddle() {
		return middle;
	}

	public char getRight() {
		return right;
	}
	
}
